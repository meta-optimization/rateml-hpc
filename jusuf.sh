#!/usr/bin/env bash

##SBATCH -A [!!enter your project here!!]
#SBATCH -A icei-hbp-2021-0003
#SBATCH --partition=gpus
#SBATCH --gres=gpu:1
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --time=04:30:00
#SBATCH -J RateML


# Run the Zerlaut AdEx
# srun python $PROJECT/$USER/tvb-root/tvb_library/tvb/rateML/run/model_driver_zerlaut.py -s0 5 -s1 1 -s2 1 -s3 1 -s4 1 -n 500 -dt .1 -sm 4 -v -w -x7
# Run the MontbrioPaxinRosin model
srun python $PROJECT/$USER/tvb-root/tvb_library/tvb/rateML/run/model_driver_montbrio.py -s0 5 -n 400 -dt .1 -sm 3 -v -w -r 76