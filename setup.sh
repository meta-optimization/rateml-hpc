#!/usr/bin/env bash

git clone -b rateml_ebrains_fix --single-branch -v https://github.com/the-virtual-brain/tvb-root.git $PROJECT/$USER/tvb-root

ml Stages/2022 GCCcore/.11.2.0
ml PyCUDA/2021.1
ml SciPy-Stack
ml numba
export PYTHONPATH=$PYTHONPATH:$PROJECT/$USER/tvb-root/tvb_library

pip install tqdm --user
pip install tvb-data --user