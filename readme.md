This TVB RateML tutorial contains 4 notebooks with self explanatory titles. 
- 0_Install_TVB.ipynb: sets up the local and remote (on a GPU enabled supercomputer) TVB installation. 
- 1_RateML_ModGen.ipynb: executes a ratemls code generator for local python or remote cuda models for TVB.
- 2_Simulate_Local.ipynb: enables simulation of the generated .py models in the Ebrains environment.
- 3_Simulate_HPC.ipynb" enables simulation of the generated .c models on a HPC environment.

For a remote installation of TVB on one of the HPC sites, one needs to register [here](https://judoor.fz-juelich.de/register). Once registered a project with compute time can be acquired via the fenix infrastructure of which details can be found [here](https://wiki.ebrains.eu/bin/view/Collabs/fenix-icei/). 
If one wants to do only local simulations with rateml generated python models, one should do the first 3 steps of 0_Install_TVB.ipynb and discard 3_Simulate_HPC.ipynb. 
The rateml outputted cuda driver and model can be run standalone on any CUDA GPU capable (virtual) machine. For more information check out the [readme](https://github.com/the-virtual-brain/tvb-root/blob/master/tvb_library/tvb/rateML/README.md).