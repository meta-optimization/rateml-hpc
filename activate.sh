#module use $OTHERSTAGES
#module load Stages/2020

jutil env activate -p icei-hbp-2021-0003

ml Stages/2022 GCCcore/.11.2.0
ml PyCUDA/2021.1
ml SciPy-Stack
ml numba
export PYTHONPATH=$PYTHONPATH:/p/project/training2221/$USER/tvb-root/tvb_library


#export PYTHONPATH=$PYTHONPATH:/p/project/cslns/vandervlag1/uniRate/tvb-root/scientific_library/
#ml SciPy-Stack/2020-Python-3.8.5
#ml numba
#export PYTHONPATH=$PYTHONPATH:/p/project/cslns/vandervlag1/uniRate/tvb-root/scientific_library/
